<?php

namespace app\game\controller;

use think\Controller;
use think\Loader;
use think\Db;
use think\Request;

class Gscore extends Controller {

    public function __construct() {
        parent::__construct();
    }

    //创建房间
    public function getGroupMemberList() {
        $dataPost = json_decode(file_get_contents('php://input'), true);
        //获取用户ID
        $account_id = isset($dataPost['account_id']) ? $dataPost['account_id'] : null;
        //获取房间号
        $page = isset($dataPost['page']) ? $dataPost['page'] : null;
        $dealer_num = isset($dataPost['dealer_num']) ? $dataPost['dealer_num'] : null;
        $type = isset($dataPost['type']) ? $dataPost['type'] : null;
        $inviterCount = DB::query("select id from t_groups where account_id=:account_id ", ['account_id' => $account_id]);
        $allCount = count($inviterCount);
        $limit = 15;

        $sum_page = ceil($allCount / 15);
        $limitEnd = $limit * $page;
        $limitStart = $limitEnd - 15;
        //获取数据
        $inviter = DB::query("select account_id as account_code,nickname,headimgurl,status from t_groups where account_code=:account_code order by creatime asc limit " . $limitStart . "," . $limitEnd, ['account_code' => $account_id]);

        $result['result'] = 0;
        $result['data'] = $inviter;
        $result['result_message'] = "获取玩家战绩列表";
        $result['sum_page'] = $sum_page;
        $result['page'] = $page;
        echo json_encode($result);
    }

    public function dealMemberApply() {
        $dataPost = json_decode(file_get_contents('php://input'), true);
        $account_code = isset($dataPost['account_id']) ? $dataPost['account_id'] : null;
        $account_id = isset($dataPost['apply_code']) ? $dataPost['apply_code'] : null;
        $dealer_num = isset($dataPost['dealer_num']) ? $dataPost['dealer_num'] : null;
        $type = isset($dataPost['type']) ? $dataPost['type'] : null;
        $inviterInfo = DB::query("select * from t_groups where account_id=:account_id ", ['account_id' => $account_code]);
        if (isset($inviterInfo[0])) {
            if ($type == 1) {
                $inviterUpdate = DB::query("update t_groups set status=2  where account_id=:account_id  and account_code=:account_code", ['account_id' => $account_id,'account_code' => $account_code]);
            } elseif ($type == 2) {
                 $inviterUpdate = Db::table('t_groups')->where(array('account_id' => $account_id,'account_code' => $account_code))->delete();
            } elseif ($type == 3) {
                 $inviterUpdate = Db::table('t_groups')->where(array('account_id' => $account_id,'account_code' => $account_code))->delete();
            }
        }
        $result['result'] =0;
        $result['data'] =array();
        $result['result_message'] ="操作成功";
        echo json_encode($result);
    }



    public function applyJoinGroup() {
        $dataPost = json_decode(file_get_contents('php://input'), true);
        if (!isset($dataPost['dealer_num']) || !isset($dataPost['code']) || !isset($dataPost['account_id'])) {
            $result['result'] = 1;
            $result['data'] = array();
            $result['result_message'] = "申请失败";
            echo json_encode($result);
            exit;
        }
        $inviter = DB::query('select * from t_groups where account_code=:code and group_winer=1 limit 1', ['code' => $dataPost['code']]);
        if (!$inviter || !isset($inviter[0])) {
            $result['result'] = 1;
            $result['data'] = array();
            $result['result_message'] = "申请失败";
        } else {
            $memberInfo = DB::query('select nickname,photo from t_member where id=:account_id limit 1', ['account_id' => $dataPost['account_id']]);

            $inviterInfo = DB::query('select * from t_groups where account_code=:code and account_id=:account_id limit 1', ['code' => $dataPost['code'], 'account_id' => $dataPost['account_id']]);
            if (!isset($inviterInfo[0]) && isset($memberInfo[0]['nickname'])) {
                $memSelf = [
                    'account_id' => $dataPost['account_id'],
                    'group_winer' => 0,
                    'account_code' => $dataPost['code'],
                    'nickname' => $memberInfo[0]['nickname'],
                    'headimgurl' => $memberInfo[0]['photo'],
                    'status' => 1,
                    'creatime' => time()
                ];
                Db::table('t_groups')->insert($memSelf);
                $result['result'] = 0;
                $result['data'] = array();
                $result['result_message'] = "申请成功，等待管理员同意";
            } elseif (isset($inviterInfo[0]) && $inviterInfo[0]['status'] != 1) {
                $result['result'] = 0;
                $result['data'] = array();
                $result['result_message'] = "你已加入该群组";
            } else {
                $result['result'] = 0;
                $result['data'] = array();
                $result['result_message'] = "申请成功，等待管理员同意";
            }
        }

        echo json_encode($result);
    }

	public function getRoomList(){
		$account_id = (int)input('post.account_id');
		$page = (int)input('post.page');
		$dealer_num = (int)input('post.dealer_num');
		$game_type = (int)input('post.game_type');
		$psize = 15;
		if(false == $account_id || false == $page || false == $dealer_num || false == $game_type){
			die(json_encode([
				"result" => 0,
				"data" => [],
				"result_message" => "获取用户开房记录",
				"sum_page" => 1,
				"page" => 1
			]));
		}
		$sum_page = Db::query('select count(*) as sum_page from t_room where account_id=:account_id and game_type=:game_type', ['account_id' => $account_id, 'game_type' => $game_type]);
		isset($sum_page['sum_page']) ? $sum_page = $sum_page['sum_page'] : $sum_page = 1;
		$limitbegin = $page * $psize - ($psize);
		$limitend = $page * $psize - 1;
		$data = Db::query(
			'select id as room_id, room_number, FROM_UNIXTIME(starttime, "%m-%d %H:%i") as create_time, 1 as status  from t_room where account_id=:account_id and game_type=:game_type and roomStatus=4 order by starttime desc limit :limitbegin, :limitend', 
			['account_id' => $account_id, 'game_type' => $game_type, 'limitbegin' => $limitbegin, 'limitend' => $limitend]
		);
		if(false == $data) $data = [];
		die(json_encode([
				"result" => 0,
				"data" => $data,
				"result_message" => "获取用户开房记录",
				"sum_page" => $sum_page,
				"page" => $page
		]));
	}
}
