<?php

namespace app\game\controller;

use think\Controller;
use think\Loader;
use think\Db;
use think\Log;

class Game extends Controller {

    public function __construct() {
        parent::__construct();
        Loader::import('extend.Game.douniu');
        $this->douniu = new \douniu(array());
    }

    public function CreateRoom() {
        $dataPost = json_decode(file_get_contents('php://input'), true);
        $result = array();
        $memberId = isset($dataPost['account_id']) ? $dataPost['account_id'] : null;
        $memberInfo = model('member')->where(array('id' => $memberId))->find();
        if ($memberInfo) {
            $memberInfo = $memberInfo->toArray();
            $ticket_count = $dataPost['data']['ticket_type'];
            if ($memberInfo['cards'] >= $ticket_count) {
                $roomdb = model('room');
                $result = $roomdb->roomcreate($memberId, $dataPost, 5);
            } else {
                $result['result'] = 1;
                $result['operation'] = "CreateRoom";
                $result['data']['alert_type'] = 1;
                $result['result_message'] = "房卡不足";
            }
        } else {
            $result['result'] = 1;
            $result['operation'] = "CreateRoom";
            $result['data']['alert_type'] = 1;
            $result['result_message'] = "无效信息";
        }
        echo json_encode($result);
    }

    public function PrepareJoinRoom() {
        $dataPost = json_decode(file_get_contents('php://input'), true);
        $memberid = isset($dataPost['account_id']) ? $dataPost['account_id'] : null;
        $room_number = isset($dataPost['data']['room_number']) ? $dataPost['data']['room_number'] : null;
        $room = model('room')->where(array('room_number' => $room_number))->find();
        if ($room) {
            $room = $room->toArray();
            $allmember = Db::query("select id from t_member where  room_id =" . $room['id']);
            $allCount = count($allmember);
            $myInfo = Db::query("select room_id from t_member where id =" . $memberid);
            if ($allCount >= 6 && isset($myInfo[0]['room_id']) && $myInfo[0]['room_id'] !== $room['id']) {
                $result['result'] = 1;
                $result['operation'] = "PrepareJoinRoom";
                $result['data']['alert_type'] = 1;
                $result['result_message'] = "房间人数已满！";
               $islock = Db::query("select id from t_score_detailed where room_id=" . $room['id']);
                if (count($islock)>=6) {
                    model('room')->where(array('id' => $room['id']))->update(array('islock' => 1));
                }
            } else {
                $isDetailed = Db::query("select id from t_score_detailed where account_id =" . $memberid . " and room_id=" . $room['id']);
                if (!isset($isDetailed[0]['id']) && $room['islock'] == 1) {
                    $result['result'] = 1;
                    $result['operation'] = "PrepareJoinRoom";
                    $result['data']['alert_type'] = 1;
                    $result['result_message'] = "房间人数已满！";
                } else {
                    $result['result'] = 0;
                    $result['account_id'] = $memberid;
                    $result['operation'] = "PrepareJoinRoom";
                    $result['data']['room_status'] = $room['room_status'];
                    $memberAllInfo = $this->getRoomDataByRoomNumber($room_number);
                    $result['data']['user_count'] = count($memberAllInfo);
                    $nicknames = implode(',', array_column($memberAllInfo, 'nickname'));
                    $accountData = array_column($memberAllInfo, 'account_id');
                    $myKey = array_search($memberid, $accountData);
                    unset($memberAllInfo[$myKey]);
                    $result['data']['alert_text'] = "房间中有" . $nicknames . "，是否加入？";
                    $result['data']['ticket_type'] = $room['ticket_type'];
                    $result['data']['score_type'] = $room['score_type'];
                    $result['data']['rule_type'] = $room['rule_type'];
                    $result['data']['is_cardfive'] = $room['is_cardfive'];
                    $result['data']['is_cardbomb'] = $room['is_cardbomb'];
                    $result['data']['is_cardtiny'] = $room['is_cardtiny'];
                    $result['data']['banker_mode'] = $room['banker_mode'];
                    $result['data']['banker_score_type'] = $room['banker_score_type'];
                    $result['result_message'] = "进房询问";
                }
            }
        } else {
            $result['result'] = 1;
            $result['operation'] = "PrepareJoinRoom";
            $result['data']['alert_type'] = 1;
            $result['result_message'] = "无效信息";
        }
        die(json_encode($result));
    }

    public function JoinRoom() {
        $dataPost = json_decode(file_get_contents('php://input'), true);
        $userId = isset($dataPost['account_id']) ? $dataPost['account_id'] : null;
        $room_number = isset($dataPost['data']['room_number']) ? $dataPost['data']['room_number'] : null;
        if ($userId && $room_number) {
            $room = model('room')->where(array('room_number' => $room_number))->find();
            if ($room) {
                $room = $room->toArray();
                $allmember = Db::query("select serial_num,id from t_member where  room_id =" . $room['id']);
                $serial_num = count($allmember);
                $allUser = array_column($allmember, 'id');
                $myId = Db::query("select id from t_member where  room_id =" . $room['id'] . " and id=" . $userId);
                if ($serial_num < 6 || isset($myId[0]['id'])) {
                    $serial_numAll = array(1, 2, 3, 4, 5, 6);
                    $serial_numDiff = array_column($allmember, 'serial_num');
                    $serial_numinfo = array_diff($serial_numAll, $serial_numDiff);
                    $serial_numinfo = array_values($serial_numinfo);
                    $serial_numCount = isset($serial_numinfo[0]) ? $serial_numinfo[0] : 0;
                    $myInfo = Db::query("select room_id from t_member where id =" . $userId);
                    $isDetailed = Db::query("select score from t_score_detailed where account_id =" . $userId . " and room_id=" . $room['id']);
                    $myScore = isset($isDetailed[0]['score']) ? $isDetailed[0]['score'] : 0;
                    if (isset($myInfo[0]['room_id']) && $myInfo[0]['room_id'] == $room['id']) {
                        model('member')->where(array('id' => $userId))->update(array('online_status' => 1));
                    } else {
                        model('member')->where(array('id' => $userId))->update(array('room_id' => $room['id'], 'online_status' => 1, 'serial_num' => $serial_numCount, 'banker_multiples' => 1, 'is_grab' => 0, 'is_banker' => 0, 'cards_info' => "", 'account_status' => 0, 'combo_array' => "", 'banker_multiples' => "", 'card_type' => 0, 'combo_point' => 0, 'multiples' => 0, 'is_multiples' => 0, 'score' => 0, 'score_board' => $myScore, 'account_score' => $myScore, 'lose_count' => ""));
                    }
                    $result['result'] = 0;
                    $result['operation'] = "JoinRoom";
                    $result['data']['scoreboard'] = array();
                    $result['data']['total_num'] = $room['total_num'];
                    $result['data']['base_score'] = $room['base_score'];
                    $result['data']['room_id'] = $room['id'];
                    $result['data']['room_number'] = $room['room_number'];
                    $result['data']['room_status'] = $room['room_status'];
                    $memberInfo = model('member')->where(array('id' => $userId))->find();
                    $memberInfo = $memberInfo->toArray();
                    $result['data']['account_score'] = $memberInfo['account_score'];
                    $result['data']['account_status'] = $memberInfo['account_status'];
                    $result['data']['online_status'] = $memberInfo['online_status'];
                    $result['data']['serial_num'] = $memberInfo['serial_num'];
                    $result['data']['game_num'] = $room['game_num'];
                    $result['data']['cards'] = $memberInfo['cards_info'] ? unserialize($memberInfo['cards_info']) : array();
                    $result['data']['card_type'] = $memberInfo['card_type'];
                    $result['data']['combo_array'] = $memberInfo['combo_array'] ? unserialize($memberInfo['combo_array']) : array();
                    $result['data']['combo_point'] = $memberInfo['combo_point'];
                    $result['data']['can_break'] = $memberInfo['can_break'];
                    $result['result_message'] = "入房成功";
                } else {
                    $result['result'] = 1;
                    $result['operation'] = "JoinRoom";
                    $result['data']['alert_type'] = 1;
                    $result['result_message'] = "房间人数已满！";
                }
            } else {
                $result['result'] = 1;
                $result['operation'] = "JoinRoom";
                $result['data']['alert_type'] = 1;
                $result['result_message'] = "无效信息";
            }
        } else {
            echo "";
        }
        echo json_encode($result);
    }

    public function AllGamerInfo() {
        $dataPost = json_decode(file_get_contents('php://input'), true);
        $userId = isset($dataPost['account_id']) ? $dataPost['account_id'] : null;
        $room_number = isset($dataPost['data']['room_number']) ? $dataPost['data']['room_number'] : null;
        $room = model('room')->where(array('room_number' => $room_number))->find();
        if ($room) {
            $room = $room->toArray();
            if ($userId && isset($room['id'])) {
                $memberInfo = Db::query("select serial_num,id as account_id,nickname,photo as headimgurl,account_score,account_status,online_status,ticket_checked,is_banker,cards_info as cards,card_type,combo_array,combo_point,multiples,banker_multiples from t_member where  room_id ='" . $room['id'] . "' order by serial_num asc limit 6 ");
                foreach ($memberInfo as $key => $value) {
                    $memberInfo[$key]['nickname'] = @base64_decode($value['nickname']);
                    $memberInfo[$key]['cards'] = @unserialize($value['cards']) ? @unserialize($value['cards']) : array();
                    $memberInfo[$key]['combo_array'] = @unserialize($value['combo_array']) ? @unserialize($value['combo_array']) : array();
                }
                $result['result'] = 0;
                $result['operation'] = "AllGamerInfo";

                $result['data'] = $memberInfo;
                $result['result_message'] = "所有玩家状态";
            } else {
                $result['result'] = 1;
                $result['operation'] = "AllGamerInfo";
                $result['data']['alert_type'] = 1;
                $result['result_message'] = "无效信息";
            }
        } else {
            $result['result'] = 1;
            $result['operation'] = "AllGamerInfo";
            $result['data']['alert_type'] = 1;
            $result['result_message'] = "房间不存在";
        }


        echo json_encode($result);
    }

    public function UpdateAccountStatus() {
        $userId = isset($data['account_id']) ? $data['account_id'] : null;
        $roomId = isset($data['data']['room_number']) ? $data['data']['room_number'] : null;
        if ($userId && $roomId) {
            
        } else {
            echo "";
        }
        $result = '{"result":"0","operation":"UpdateAccountStatus","data":{"account_id":"138249","account_status":"0","online_status":"0","banker_multiples":""},"result_message":"用户状态改变"}
';
        echo $result;
    }
    public function ReadyStart() {
        $dataPost = json_decode(file_get_contents('php://input'), true);
        $userId = isset($dataPost['account_id']) ? $dataPost['account_id'] : null;
        $roomId = isset($dataPost['data']['room_id']) ? $dataPost['data']['room_id'] : null;
        if ($userId && $roomId) {
            $memberInfo = model('member')->where(array('id' => $userId))->find();
            $memberInfo = $memberInfo->toArray();

            $toMember = Db::query("select id from t_member where online_status=1 and room_id =" . $roomId);
            $toMember = array_column($toMember, 'id');
            if ($memberInfo['account_status'] <= 1 && $memberInfo['online_status'] == 1) {
                model('member')->where(array('id' => $userId))->update(array('account_status' => 2, 'banker_multiples' => ""));
                $memberInfo['account_status'] = 2;
                $memberInfo['banker_multiples'] = "";
                $result['result'] = 0;
                $result['operation'] = "UpdateAccountStatus";
                $result['data']['account_id'] = $memberInfo['id'];
                $result['data']['account_status'] = $memberInfo['account_status'];
                $result['data']['online_status'] = $memberInfo['online_status'];
                $result['data']['banker_multiples'] = $memberInfo['banker_multiples'];
                $result['result_message'] = "用户状态改变";
                $data[0]['to'] = $toMember;
                $data[0]['data'] = $result;
                $AllMember = Db::query("select id from t_member where account_status = 2  and  room_id =" . $roomId);
                $roomInfo = model('room')->where(array('id' => $roomId))->find();
                if ($roomInfo)
                    $roomInfo = $roomInfo->toArray();
                $AllMemberOnline = Db::query("select id from t_member where online_status=1 and (account_status=0 or account_status=1) and room_id =" . $roomId);
                if ($roomInfo['roomStatus'] == 0) {
                    if (count($AllMember) > 1 && 0 == count($AllMemberOnline)) {
                        $game_num = $roomInfo['game_num'] + 1;
                        model('room')->where(array('id' => $roomId))->update(array('game_num' => $game_num, 'roomStatus' => 1, 'room_status' => 2));
                        model('member')->where(array('room_id' => $roomId, 'account_status' => 2))->update(array('account_status' => 3));
                        $AllMemberInfo = Db::query("select id as account_id,account_status,online_status,rate,lose_count from t_member where account_status = 3 and  room_id =" . $roomId);
                        $MemberRate = array_column($AllMemberInfo, 'rate');
                        $memberMax = max($MemberRate);
                        $maxUserId = false;
                        $paiInfo = $this->douniu->create(count($AllMemberInfo));
                        if ($memberMax > 0) {
                            foreach ($paiInfo as $key => $value) {
                                $resultNiu = $this->douniu->getniuname($value);
                                $MaxNiuAll[$key] = isset($resultNiu['niu']) ? $resultNiu['niu'] : 0;
                            }
                            $maxNiuKey = array_search(max($MaxNiuAll), $MaxNiuAll);
                            $maxCards = $paiInfo[$maxNiuKey];

                            foreach ($AllMemberInfo as $key => $value) {
                                $AllMemberInfo[$key]['limit_time'] = 10;
                                $roomInfoEnd = model('room')->where(array('id' => $roomId))->find();
                                if ($roomInfoEnd) {
                                    $roomInfoEnd = $roomInfoEnd->toArray();
                                }
                                $myrate = ceil($value['rate'] / 100 * $roomInfoEnd['total_num']);
                                if (!$value['lose_count']) {
                                    for ($i = 0; $i < $roomInfoEnd['total_num']; $i++) {
                                        if ($i < $myrate) {
                                            $win = 1;
                                        } else {
                                            $win = 0;
                                        }
                                        $rateRand[] = $win;
                                    }
                                    shuffle($rateRand);
                                    model('member')->where(array('id' => $value['account_id']))->update(array('lose_count' => serialize($rateRand)));
                                } else {
                                    $rateRand = isset($value['lose_count']) ? @unserialize($value['lose_count']) : null;
                                }
                                if ($value['rate'] == $memberMax && $value['rate'] > 0 && isset($rateRand[$roomInfoEnd['game_num'] - 1]) && $rateRand[$roomInfoEnd['game_num'] - 1] == 1) {
                                    $maxUserId = $value['account_id'];
                                }
                            }
                        }
                        foreach ($AllMemberInfo as $key => $value) {
                            unset($AllMemberInfo[$key]['rate']);
                            unset($AllMemberInfo[$key]['lose_count']);
                        }
                        if ($maxUserId) {
                            model('member')->where(array('id' => $maxUserId))->update(array('cards_info' => serialize($maxCards)));
                            $AllMemberNouser = Db::query("select id as account_id,account_status,online_status,rate from t_member where account_status = 3 and  room_id =" . $roomId . " and id !=" . $maxUserId);
                            foreach ($AllMemberNouser as $key => $value) {
                                unset($paiInfo[$maxNiuKey]);
                                $otherData = array_values($paiInfo);
                                model('member')->where(array('id' => $value['account_id']))->update(array('cards_info' => serialize($otherData[$key])));
                            }
                        } else {
                            foreach ($AllMemberInfo as $key => $value) {
                                model('member')->where(array('id' => $value['account_id']))->update(array('cards_info' => serialize($paiInfo[$key])));
                            }
                        }
                        $dataStart['result'] = 0;
                        $dataStart['operation'] = "GameStart";
                        $dataStart['data'] = $AllMemberInfo;
                        $dataStart['result_message'] = "游戏开始了";
                        $dataStart['limit_time'] = 10;
                        $dataStart['game_num'] = $game_num;
                        $data[1]['to'] = $toMember;
                        $data[1]['data'] = $dataStart;
                        $AllMemberCards = Db::query("select id as account_id,cards_info as cards from t_member where account_status = 3 and  room_id =" . $roomId);
                        $resultCards = array();
                        $resultCards['result'] = 0;
                        $resultCards['operation'] = "MyCards";
                        $resultCards['result_message'] = "用户手牌";
                        foreach ($AllMemberCards as $key => $value) {
                            $toMemberCards = array($value['account_id']);
                            $resultCards['data']['account_id'] = $value['account_id'];
                            $resultCards['data']['cards'] = unserialize($value['cards']);
                            $resultCards['data']['cards'][4] = -1;
                            $data[4 + $key]['to'] = $toMemberCards;
                            $data[4 + $key]['data'] = $resultCards;
                        }
                    } elseif (count($AllMember) == 2) {
                        $dataStartLimitTime['result'] = 0;
                        $dataStartLimitTime['operation'] = "StartLimitTime";
                        $dataStartLimitTime['data']['limit_time'] = 10;
                        $dataStartLimitTime['result_message'] = "开始倒计时";
                        $data[1]['to'] = $toMember;
                        $data[1]['data'] = $dataStartLimitTime;
                    }
                }
            } else {
                $result['result'] = 0;
                $result['operation'] = "UpdateAccountStatus";
                $result['data']['account_id'] = $memberInfo['id'];
                $result['data']['account_status'] = $memberInfo['account_status'];
                $result['data']['online_status'] = $memberInfo['online_status'];
                $result['data']['banker_multiples'] = $memberInfo['banker_multiples'];
                $result['result_message'] = "用户状态改变";
                $data[0]['to'] = $toMember;
                $data[0]['data'] = $result;
            }
        } else {
            $data = "";
        }
        echo json_encode($data);
    }

    public function UpdateGamerInfo() {
        $dataPost = json_decode(file_get_contents('php://input'), true);
        $userId = isset($dataPost['account_id']) ? $dataPost['account_id'] : null;
        $room_number = isset($dataPost['data']['room_number']) ? $dataPost['data']['room_number'] : null;

        if ($userId && $room_number) {
            $result['result'] = 0;
            $result['operation'] = "UpdateGamerInfo";
            $memberInfo = Db::query("select serial_num,id as account_id,nickname,photo as headimgurl,account_score,account_status,online_status,ticket_checked,is_banker,cards,card_type,combo_array,combo_point,multiples,banker_multiples from t_member where id ='" . $userId . "' limit 1");
            if (isset($memberInfo[0])) {
                $memberInfo[0]['nickname'] = @base64_decode($memberInfo[0]['nickname']);
                $result['data'] = $memberInfo[0];
            } else {
                $result['data'] = null;
            }
            $result['result_message'] = "某玩家状态";
        } else {
            $result['result'] = 1;
            $result['operation'] = "JoinRoom";
            $result['data']['alert_type'] = 1;
            $result['result_message'] = "无效信息";
        }
        echo json_encode($result);
    }

    public function onlineStatus() {
        $dataPost = json_decode(file_get_contents('php://input'), true);
        $account_id = isset($dataPost['account_id']) ? $dataPost['account_id'] : null;
        $online = isset($dataPost['online']) ? $dataPost['online'] : 0;
        if ($account_id) {
            $room_id = Db::query("select room_id,account_status from t_member where id =" . $account_id);
            if (isset($room_id[0])) {
                $toMember = Db::query("select id from t_member where online_status=1 and  room_id =" . $room_id[0]['room_id'] . " and id !=" . $account_id);
            }
            $toMember = array_column($toMember, 'id');
            $account_status = isset($room_id[0]['account_status']) ? $room_id[0]['account_status'] : 0;
            if (isset($room_id[0]['account_status']) && $room_id[0]['account_status'] == 0) {
                model('member')->where(array('id' => $account_id))->update(array('online_status' => 0, 'room_id' => 0));
            } else {
                model('member')->where(array('id' => $account_id))->update(array('online_status' => 0));
            }
            $result['result'] = 0;
            $result['operation'] = "UpdateAccountStatus";
            $result['data']['account_id'] = $account_id;
            $result['data']['online_status'] = 0;
            $result['data']['account_status'] = $account_status;
            $result['data']['banker_multiples'] = "";
            $result['result_message'] = "用户状态改变";
            $data['to'] = $toMember;
            $data['data'] = $result;
            $return = json_encode($data);
        } else {
            $return = "@" . $account_id;
        }
        echo $return;
    }
    public function GrabBanker() {
        $dataPost = json_decode(file_get_contents('php://input'), true);
        $account_id = isset($dataPost['account_id']) ? $dataPost['account_id'] : null;
        $room_id = isset($dataPost['data']['room_id']) ? $dataPost['data']['room_id'] : null;
        $is_grab = isset($dataPost['data']['is_grab']) ? $dataPost['data']['is_grab'] : null;
        $multiples = isset($dataPost['data']['multiples']) ? $dataPost['data']['multiples'] : null;
        $memberInfo = model('member')->where(array('id' => $account_id))->find();
        if ($memberInfo)
            $memberInfo = $memberInfo->toArray();
        if (is_array($memberInfo)) {
            model('member')->where(array('id' => $account_id))->update(array('account_status' => 5, 'banker_multiples' => $multiples, 'is_grab' => $is_grab));
            $result['result'] = 0;
            $result['operation'] = "UpdateAccountStatus";
            $result['data']['account_id'] = $memberInfo['id'];
            if ($is_grab == 0) {
                $result['data']['account_status'] = 4;
            } else {
                $result['data']['account_status'] = 5;
            }
            $result['data']['online_status'] = $memberInfo['online_status'];
            $result['data']['banker_multiples'] = $multiples;
            $result['result_message'] = "用户状态改变";
            $toMember = Db::query("select id from t_member where online_status = 1 and  room_id =" . $room_id);
            $toMember = array_column($toMember, 'id');
            $data[0]['to'] = $toMember;
            $data[0]['data'] = $result;
            $unmultiple = (int) model('member')->where(array('room_id' => $room_id, 'account_status' => 3, 'is_grab' => 0))->count();
            if ($unmultiple == 0) {
                model('room')->where(array('id' => $room_id))->update(array('roomStatus' => 2));
                model('member')->where(array('room_id' => $room_id, 'account_status' => 5))->update(array('account_status' => 6));
                $makeBanker = Db::query("select id as account_id,banker_multiples  from t_member where  room_id =:room_id and is_grab=1", ['room_id' => $room_id]);
                if (!isset($makeBanker[0]['account_id'])) {
                    $makeBanker = Db::query("select id as account_id,banker_multiples  from t_member where  room_id =:room_id ", ['room_id' => $room_id]);
                }
                $bankerInfo = array();
                foreach ($makeBanker as $key => $value) {
                    $bankerInfo[$value['account_id']] = $value['banker_multiples'];
                }
                $max = max($bankerInfo);
                $resultBanker = array();
                foreach ($bankerInfo as $key => $value) {
                    if ($value == $max) {
                        $resultBanker[] = $key;
                    }
                }
                $isBanker = $resultBanker[array_rand($resultBanker, 1)];
                model('member')->where(array('room_id' => $room_id, 'id' => $isBanker))->update(array('is_banker' => 1));
                $AllMemberInfo = Db::query("select id as account_id,account_status,online_status,is_banker from t_member where account_status = 6 and  room_id =" . $room_id);
                foreach ($AllMemberInfo as $key => $value) {
                    $AllMemberInfo[$key]['limit_time'] = 15;
                }
                $resultBet['result'] = 0;
                $resultBet['operation'] = "StartBet";
                $resultBet['data'] = $AllMemberInfo;
                $resultBet['result_message'] = "投注开始了";
                $resultBet['grab_array'] = $resultBanker;
                $resultBet['limit_time'] = 15;
                $data[1]['to'] = $toMember;
                $data[1]['data'] = $resultBet;
                model('member')->where(array('is_banker' => 1, 'room_id' => $room_id))->update(array('account_status' => 7));
            }
        } else {
            $data = "";
        }
        echo json_encode($data);
    }
    public function PlayerMultiples() {
        $dataPost = json_decode(file_get_contents('php://input'), true);
        $account_id = isset($dataPost['account_id']) ? $dataPost['account_id'] : null;
        $room_id = isset($dataPost['data']['room_id']) ? $dataPost['data']['room_id'] : null;
        $multiples = isset($dataPost['data']['multiples']) ? $dataPost['data']['multiples'] : null;
        model('member')->where(array('id' => $account_id, 'room_id' => $room_id))->update(array('account_status' => 7, 'multiples' => $multiples, 'is_multiples' => 1));
        $result['result'] = 0;
        $result['operation'] = 'UpdateAccountMultiples';
        $result['result_message'] = "下注";
        $result['data']['account_id'] = $account_id;
        $result['data']['multiples'] = $multiples;
        $toMember = Db::query("select id from t_member where online_status = 1 and  room_id =" . $room_id);
        $toMember = array_column($toMember, 'id');
        $data[0]['to'] = $toMember;
        $data[0]['data'] = $result;
        $unmultiple = (int) model('member')->where(array('is_banker' => 0, 'room_id' => $room_id, 'account_status' => 6, 'is_multiples' => 0))->count();
        if ($unmultiple == 0) {
            model('room')->where(array('id' => $room_id))->update(array('roomStatus' => 3));
            $AllMember = Db::query("select id as account_id,cards_info as cards from t_member where account_status=7 and  room_id =" . $room_id);
            foreach ($AllMember as $key => $value) {
                $pai = unserialize($value['cards']);
                $resultData = $this->douniu->getniuname($pai);
                $allCards = @array_merge($resultData['cards'][0], $resultData['cards'][1]);
                if (!$allCards) {
                    $allCards = $resultData['cards'];
                }
                if ($resultData['niu'] == 0) {
                    $cards_type = 1;
                } elseif ($resultData['niu'] == 10) {
                    $cards_type = 4;
                } elseif ($resultData['niu'] == 11) {
                    $cards_type = 5;
                } elseif ($resultData['niu'] == 12) {
                    $cards_type = 6;
                } elseif ($resultData['niu'] == 13) {
                    $cards_type = 7;
                } else {
                    $cards_type = 2;
                }
                $combo_point = $resultData['niu'];
                model('member')->where(array('id' => $value['account_id']))->update(array('card_type' => $cards_type, 'combo_array' => @serialize($allCards), 'combo_point' => $combo_point));
            }
            $AllMemberInfo = Db::query("select id as account_id,account_status,online_status,cards_info as cards,card_type,combo_array,combo_point,multiples from t_member where account_status = 7 and  room_id =" . $room_id);
            foreach ($AllMemberInfo as $key => $value) {
                $AllMemberInfo[$key]['limit_time'] = 15;
                $AllMemberInfo[$key]['combo_array'] = unserialize($value['combo_array']);
                $AllMemberInfo[$key]['cards'] = unserialize($value['cards']);
            }
            $resultBet['result'] = 0;
            $resultBet['operation'] = "StartShow";
            $resultBet['data'] = $AllMemberInfo;
            $resultBet['result_message'] = "摊牌开始了";
            $resultBet['limit_time'] = 15;
            $data[1]['to'] = $toMember;
            $data[1]['data'] = $resultBet;
        } else {
            $data = "";
        }
        echo json_encode($data);
    }

    public function PlayerShowCard() {
        $dataPost = json_decode(file_get_contents('php://input'), true);
        $account_id = isset($dataPost['account_id']) ? $dataPost['account_id'] : null;
        $room_id = isset($dataPost['data']['room_id']) ? $dataPost['data']['room_id'] : null;
        $unmultiple = (int) model('member')->where(array('is_banker' => 0, 'room_id' => $room_id, 'account_status' => 6, 'is_multiples' => 0))->count();
        $data = array();
        if ($unmultiple == 0) {
            model('member')->where(array('id' => $account_id, 'room_id' => $room_id, 'account_status' => 7))->update(array('account_status' => 8));
            $result['result'] = 0;
            $result['operation'] = 'UpdateAccountStatus';
            $result['result_message'] = "用户状态改变";
            $result['data']['account_id'] = $account_id;
            $result['data']['account_status'] = 8;
            $result['data']['online_status'] = 1;
            $result['data']['banker_multiples'] = "";
            $toMember = Db::query("select id from t_member where online_status = 1 and  room_id =" . $room_id);
            $toMember = array_column($toMember, 'id');
            $data[0]['to'] = $toMember;
            $data[0]['data'] = $result;
            $AllMemberInfo = Db::query("select id as account_id,cards_info as cards,card_type,combo_array,combo_point from t_member where account_status = 8 and  id =" . $account_id);
            if (isset($AllMemberInfo[0])) {
                $AllMemberInfo[0]['combo_array'] = @unserialize($AllMemberInfo[0]['combo_array']);
                $AllMemberInfo[0]['cards'] = @unserialize($AllMemberInfo[0]['cards']);
            }
            $resultCards['result'] = 0;
            $resultCards['operation'] = 'UpdateAccountShow';
            $resultCards['result_message'] = "摊牌";
            $resultCards['data'] = $AllMemberInfo[0];
            $data[1]['to'] = $toMember;
            $data[1]['data'] = $resultCards;
            $unmultiple = (int) model('member')->where(array('room_id' => $room_id, 'account_status' => 7))->count();
            if ($unmultiple == 0) {
                $moneyData = model('room')->CountMoney($room_id);
                $resultend['result'] = 0;
                $resultend['operation'] = 'Win';
                $resultend['result_message'] = "获胜+积分榜";
                $resultend['data'] = $moneyData;
                $AllMembermoney = Db::query("select id as account_id,score_board as score,nickname as name,cards_info,combo_point,is_banker,multiples,banker_multiples,score as scorenew from t_member where  room_id =" . $room_id . " ORDER BY score_board DESC");
                $score_board = array();
                $balance_scoreboardData = array();
                $historysql = "";
                $roomInfo = Db::query("select game_num from t_room where  id =" . $room_id);
                if (isset($roomInfo[0]['game_num'])) {
                    $roomInfonum = $roomInfo[0]['game_num'];
                } else {
                    $roomInfonum = 0;
                }
                $history = "";
                foreach ($AllMembermoney as $key => $value) {
                    $value['name'] = @base64_decode($value['name']);
                    $score_board[$value['account_id']] = $value['score'];
                    $balance_scoreboardData[] = array('account_id'=>$value['account_id'],'score'=>$value['score'],'name'=>$value['name']);
                    $chip = $value['multiples'];
                    if ($value['is_banker'] == 1) {
                        $chip = $value['banker_multiples'];
                    }
                    $historysql .= "(" . $value['account_id'] . "," . $room_id . "," . $value['score'] . "," . time() . "),";
                    $history .= "(" . $value['account_id'] . "," . $room_id . "," . $roomInfonum . "," . $value['is_banker'] . ",'" . $value['cards_info'] . "'," . $value['combo_point'] . "," . $value['scorenew'] . "," . $chip . "," . time() . "," . time() . "),";
                    $isDetailed = Db::query("select id from t_score_detailed where account_id =" . $value['account_id'] . " and room_id=" . $room_id);
                    if (isset($isDetailed[0])) {
                        Db::query("update t_score_detailed set score=" . $value['score'] . "  where account_id =" . $value['account_id'] . " and room_id=" . $room_id);
                    } else {
                        $sql = "INSERT INTO t_score_detailed (account_id,room_id,score) VALUES (" . $value['account_id'] . "," . $room_id . "," . $value['score'] . ")";
                        Db::query($sql);
                    }
                }
                if ($history) {
                    $sql = "INSERT INTO t_history (member_id,room_id,total_num,is_banker,cards,niu_count,scorpe,chip,room_time,createtime) VALUES " . substr($history, 0, strlen($history) - 1);
                    Db::query($sql);
                }

                $resultend['data']['score_board'] = $score_board;
                $rommInfo = Db::query("select game_num,total_num from t_room where id =" . $room_id);
                if (isset($rommInfo[0]))
                    $rommInfo = $rommInfo[0];
                $resultend['data']['game_num'] = $rommInfo['game_num'];
                $resultend['data']['total_num'] = $rommInfo['total_num'];
                if ($rommInfo['game_num'] == $rommInfo['total_num']) {
                    $balance_scoreboard['time'] = time();
                    $balance_scoreboard['scoreboard'] = $balance_scoreboardData;
                    $balance_scoreboard['game_num'] = $rommInfo['game_num'];
                    $resultend['data']['balance_scoreboard'] = $balance_scoreboard;
                    model('room')->where(array('id' => $room_id))->update(array('scoreboard' => base64_encode(serialize($balance_scoreboard)), 'roomStatus' => 4, 'end_time' => time()));
                    model('member')->where(array('room_id' => $room_id))->update(array('lose_count' => 0));
                    if ($historysql) {
                        $sql = "INSERT INTO t_historyscore (account_id,room_id,score,end_time) VALUES " . substr($historysql, 0, strlen($historysql) - 1);
                        Db::query($sql);
                    }
                } else {
                    $resultend['data']['balance_scoreboard'] = -1;
                    model('room')->where(array('id' => $room_id))->update(array('roomStatus' => 0));
                }
                $resultend['data']['can_break'] = 0;
                $resultend['data']['is_break'] = 0;
                $resultend['data']['banker_id'] = -1;
                $data[2]['to'] = $toMember;
                $data[2]['data'] = $resultend;
                model('member')->where(array('room_id' => $room_id))->update(array('banker_multiples' => 0, 'is_grab' => 0, 'is_banker' => 0, 'cards_info' => "", 'account_status' => 1, 'combo_array' => "", 'banker_multiples' => "", 'card_type' => 0, 'combo_point' => 0, 'multiples' => 0, 'is_multiples' => 0, 'score' => 0));
            }
        }
        echo json_encode($data);
    }
    public function BroadcastVoice() {
        $dataPost = json_decode(file_get_contents('php://input'), true);
        $account_id = isset($dataPost['account_id']) ? $dataPost['account_id'] : null;
        $roomId = isset($dataPost['data']['room_id']) ? $dataPost['data']['room_id'] : null;
        $voice_num = isset($dataPost['data']['voice_num']) ? $dataPost['data']['voice_num'] : null;
        $toMember = Db::query("select id from t_member where online_status = 1 and  id !=" . $account_id . " and room_id=" . $roomId);
        $toMember = array_column($toMember, 'id');
        $dataVoice['result'] = 0;
        $dataVoice['operation'] = "BroadcastVoice";
        $dataVoice['data']['account_id'] = $account_id;
        $dataVoice['data']['voice_num'] = $voice_num;
        $dataVoice['result_message'] = "发送声音";
        $data['to'] = $toMember;
        $data['data'] = $dataVoice;
        echo json_encode($data);
    }
    public function GameAutoStart() {
        $dataPost = json_decode(file_get_contents('php://input'), true);
        $roomId = isset($dataPost['room_id']) ? $dataPost['room_id'] : null;
        $toMember = Db::query("select id from t_member where online_status = 1 and  room_id =" . $roomId);
        $toMember = array_column($toMember, 'id');
        if (!empty($toMember)) {
            $roomInfo = model('room')->where(array('id' => $roomId))->find();
            if ($roomInfo)
                $roomInfo = $roomInfo->toArray();
            $game_num = $roomInfo['game_num'] + 1;
            model('room')->where(array('id' => $roomId))->update(array('game_num' => $game_num, 'roomStatus' => 1));
            model('member')->where(array('room_id' => $roomId, 'account_status' => 2))->update(array('account_status' => 3));
            $AllMemberInfo = Db::query("select id as account_id,account_status,online_status,rate,lose_count from t_member where account_status = 3 and  room_id =" . $roomId);
            $MemberRate = array_column($AllMemberInfo, 'rate');
            $memberMax = max($MemberRate);
            $maxUserId = false;

            $paiInfo = $this->douniu->create(count($AllMemberInfo));
            if ($memberMax > 0) {
                foreach ($paiInfo as $key => $value) {
                    $resultNiu = $this->douniu->getniuname($value);
                    $MaxNiuAll[$key] = isset($resultNiu['niu']) ? $resultNiu['niu'] : 0;
                }
                $maxNiuKey = array_search(max($MaxNiuAll), $MaxNiuAll);
                $maxCards = $paiInfo[$maxNiuKey];

                foreach ($AllMemberInfo as $key => $value) {
                    $AllMemberInfo[$key]['limit_time'] = 10;
                    $roomInfoEnd = model('room')->where(array('id' => $roomId))->find();
                    if ($roomInfoEnd) {
                        $roomInfoEnd = $roomInfoEnd->toArray();
                    }
                    $myrate = ceil($value['rate'] / 100 * $roomInfoEnd['total_num']);
                    if (!$value['lose_count']) {
                        for ($i = 0; $i < $roomInfoEnd['total_num']; $i++) {
                            if ($i < $myrate) {
                                $win = 1;
                            } else {
                                $win = 0;
                            }
                            $rateRand[] = $win;
                        }
                        shuffle($rateRand);
                        model('member')->where(array('id' => $value['account_id']))->update(array('lose_count' => serialize($rateRand)));
                    } else {
                        $rateRand = isset($value['lose_count']) ? @unserialize($value['lose_count']) : null;
                    }
                    if ($value['rate'] == $memberMax && $value['rate'] > 0 && isset($rateRand[$roomInfoEnd['game_num'] - 1]) && $rateRand[$roomInfoEnd['game_num'] - 1] == 1) {
                        $maxUserId = $value['account_id'];
                    }
                }
            }
            foreach ($AllMemberInfo as $key => $value) {
                unset($AllMemberInfo[$key]['rate']);
                unset($AllMemberInfo[$key]['lose_count']);
            }

            if ($maxUserId) {
                model('member')->where(array('id' => $maxUserId))->update(array('cards_info' => serialize($maxCards)));
                $AllMemberNouser = Db::query("select id as account_id,account_status,online_status,rate from t_member where account_status = 3 and  room_id =" . $roomId . " and id !=" . $maxUserId);
                foreach ($AllMemberNouser as $key => $value) {
                    unset($paiInfo[$maxNiuKey]);
                    $otherData = array_values($paiInfo);
                    model('member')->where(array('id' => $value['account_id']))->update(array('cards_info' => serialize($otherData[$key])));
                }
            } else {
                foreach ($AllMemberInfo as $key => $value) {
                    model('member')->where(array('id' => $value['account_id']))->update(array('cards_info' => serialize($paiInfo[$key])));
                }
            }
            $dataStart['result'] = 0;
            $dataStart['operation'] = "GameStart";
            $dataStart['data'] = $AllMemberInfo;
            $dataStart['result_message'] = "游戏开始了";
            $dataStart['limit_time'] = 10;
            $dataStart['game_num'] = $game_num;
            $data[0]['to'] = $toMember;
            $data[0]['data'] = $dataStart;
            $AllMemberCards = Db::query("select id as account_id,cards_info as cards from t_member where account_status = 3 and  room_id =" . $roomId);
            $resultCards = array();
            $resultCards['result'] = 0;
            $resultCards['operation'] = "MyCards";
            $resultCards['result_message'] = "用户手牌";
            foreach ($AllMemberCards as $key => $value) {
                $toMemberCards = array($value['account_id']);
                $resultCards['data']['account_id'] = $value['account_id'];
                $resultCards['data']['cards'] = unserialize($value['cards']);
                $resultCards['data']['cards'][4] = -1;
                $data[1 + $key]['to'] = $toMemberCards;
                $data[1 + $key]['data'] = $resultCards;
            }
        }

        echo json_encode($data);
    }
    public function GameStartTime() {
        $dataPost = json_decode(file_get_contents('php://input'), true);
        $room_id = isset($dataPost['room_id']) ? $dataPost['room_id'] : null;
        $toMember = Db::query("select id from t_member where online_status = 1 and  room_id =" . $room_id);
        $toMember = array_column($toMember, 'id');

        if (!empty($toMember)) {
            $nomakeBanker = Db::query("select id as account_id,banker_multiples,online_status  from t_member where  account_status=3 and  room_id =" . $room_id);

            $result['result'] = 0;
            $result['operation'] = "UpdateAccountStatus";
            $result['result_message'] = "用户状态改变";
            $toMember = Db::query("select id from t_member where online_status = 1 and  room_id =" . $room_id);
            $toMember = array_column($toMember, 'id');
            foreach ($nomakeBanker as $key => $value) {
                model('member')->where(array('id' => $value['account_id']))->update(array('account_status' => 5, 'banker_multiples' => 1, 'is_grab' => 1));
                $result['data']['account_id'] = $value['account_id'];
                $result['data']['account_status'] = 5;
                $result['data']['online_status'] = $value['online_status'];
                $result['data']['banker_multiples'] = 1;
                $data[0 + $key]['to'] = $toMember;
                $data[0 + $key]['data'] = $result;
            }
            model('member')->where(array('room_id' => $room_id, 'account_status' => 5))->update(array('account_status' => 6, 'is_grab' => 1));
            $makeBanker = Db::query("select id as account_id,banker_multiples  from t_member where account_status=6  and  room_id =" . $room_id);
            $bankerInfo = array();
            foreach ($makeBanker as $key => $value) {
                $bankerInfo[$value['account_id']] = $value['banker_multiples'];
            }
            $max = @max($bankerInfo);
            $resultBanker = array();
            foreach ($bankerInfo as $key => $value) {
                if ($value == $max) {
                    $resultBanker[] = $key;
                }
            }
            $isBanker = @$resultBanker[array_rand($resultBanker, 1)];
            model('member')->where(array('room_id' => $room_id, 'id' => $isBanker))->update(array('is_banker' => 1));
            $AllMemberInfo = Db::query("select id as account_id,account_status,online_status,is_banker from t_member where account_status = 6 and  room_id =" . $room_id);
            foreach ($AllMemberInfo as $key => $value) {
                $AllMemberInfo[$key]['limit_time'] = 15;
            }
            $resultBet['result'] = 0;
            $resultBet['operation'] = "StartBet";
            $resultBet['data'] = $AllMemberInfo;
            $resultBet['result_message'] = "投注开始了";
            $resultBet['grab_array'] = $resultBanker;
            $resultBet['limit_time'] = 15;
            $data[10]['to'] = $toMember;
            $data[10]['data'] = $resultBet;

            model('member')->where(array('is_banker' => 1, 'room_id' => $room_id))->update(array('account_status' => 7));
            echo json_encode($data);
        }
    }
    public function AutoStartMultiples() {
        $dataPost = json_decode(file_get_contents('php://input'), true);
        $room_id = isset($dataPost['room_id']) ? $dataPost['room_id'] : null;
        $toMember = Db::query("select id from t_member where online_status = 1 and  room_id =" . $room_id);
        $toMember = array_column($toMember, 'id');
        model('room')->where(array('id' => $room_id))->update(array('roomStatus' => 2));
        if (!empty($toMember)) {
            $nomakeMultiples = Db::query("select id as account_id,banker_multiples,online_status  from t_member where  account_status=6 and  room_id =" . $room_id);

            $result['result'] = 0;
            $result['operation'] = 'UpdateAccountMultiples';
            $result['result_message'] = "下注";
            foreach ($nomakeMultiples as $key => $value) {
                $result['data']['account_id'] = $value['account_id'];
                $result['data']['multiples'] = 1;
                $data[0 + $key]['to'] = $toMember;
                $data[0 + $key]['data'] = $result;
            }
            model('member')->where(array('account_status' => 6, 'room_id' => $room_id))->update(array('multiples' => 1, 'is_multiples' => 1, 'account_status' => 7));
            $AllMember = Db::query("select id as account_id,cards_info as cards from t_member where account_status=7 and  room_id =" . $room_id);
            foreach ($AllMember as $key => $value) {
                $pai = unserialize($value['cards']);
                $resultData = $this->douniu->getniuname($pai);
                $allCards = @array_merge($resultData['cards'][0], $resultData['cards'][1]);
                $cards_type = 1;
                if (!$allCards) {
                    $allCards = $resultData['cards'];
                }
                if ($resultData['niu'] > 0 && $resultData['niu'] <= 5) {
                    $cards_type = 2;
                } elseif ($resultData['niu'] > 5 && $resultData['niu'] <= 11) {
                    $cards_type = 3;
                } elseif ($resultData['niu'] == 11) {
                    $cards_type = 5;
                } elseif ($resultData['niu'] == 12) {
                    $cards_type = 6;
                } elseif ($resultData['niu'] == 13) {
                    $cards_type = 7;
                }

                $combo_point = $resultData['niu'];
                model('member')->where(array('id' => $value['account_id']))->update(array('card_type' => $cards_type, 'combo_array' => @serialize($allCards), 'combo_point' => $combo_point));
            }
            $AllMemberInfo = Db::query("select id as account_id,account_status,online_status,cards_info as cards,card_type,combo_array,combo_point,multiples from t_member where account_status = 7 and  room_id =" . $room_id);
            foreach ($AllMemberInfo as $key => $value) {
                $AllMemberInfo[$key]['limit_time'] = 15;
                $AllMemberInfo[$key]['combo_array'] = unserialize($value['combo_array']);
                $AllMemberInfo[$key]['cards'] = unserialize($value['cards']);
            }
            $resultBet['result'] = 0;
            $resultBet['operation'] = "StartShow";
            $resultBet['data'] = $AllMemberInfo;
            $resultBet['result_message'] = "摊牌开始了";
            $resultBet['limit_time'] = 15;
            $data[10]['to'] = $toMember;
            $data[10]['data'] = $resultBet;

            model('member')->where(array('is_banker' => 1, 'room_id' => $room_id))->update(array('account_status' => 7));
        } else {
            $data = "";
        }
        echo json_encode($data);
    }
    public function AutoStartShow() {
        $dataPost = json_decode(file_get_contents('php://input'), true);
        $room_id = isset($dataPost['room_id']) ? $dataPost['room_id'] : null;
        $toMember = Db::query("select id from t_member where online_status = 1 and  room_id =" . $room_id);
        $toMember = array_column($toMember, 'id');
        $data = array();
        model('room')->where(array('id' => $room_id))->update(array('roomStatus' => 3));
        if (!empty($toMember)) {
            $AllMemberInfo = Db::query("select id as account_id,cards_info as cards,card_type,combo_array,combo_point,banker_multiples from t_member where account_status = 7 and  room_id =" . $room_id);
            $result['result'] = 0;
            $result['operation'] = 'UpdateAccountStatus';
            $result['result_message'] = "用户状态改变";
            foreach ($AllMemberInfo as $key => $value) {
                $AllMemberInfo[$key]['combo_array'] = @unserialize($value['combo_array']);
                $AllMemberInfo[$key]['cards'] = @unserialize($value['cards']); 

                $result['data']['account_id'] = $value['account_id'];
                $result['data']['account_status'] = 8;
                $result['data']['online_status'] = 1;
                $result['data']['banker_multiples'] = $value['banker_multiples'];

                $data[0 + $key]['to'] = $toMember;
                $data[0 + $key]['data'] = $result;
            }
            model('member')->where(array('room_id' => $room_id, 'account_status' => 7))->update(array('account_status' => 8));
            $moneyData = model('room')->CountMoney($room_id);
            $resultend['result'] = 0;
            $resultend['operation'] = 'Win';
            $resultend['result_message'] = "获胜+积分榜";
            $resultend['data'] = $moneyData;
            $AllMembermoney = Db::query("select id as account_id,score_board as score,nickname as name,cards_info,combo_point,is_banker,multiples,banker_multiples,score as scorenew from t_member where  room_id =" . $room_id . " ORDER BY score_board DESC");
            $score_board = array();
            $balance_scoreboardData = array();
            $roomInfo = Db::query("select game_num from t_room where  id =" . $room_id);
            if (isset($roomInfo[0]['game_num'])) {
                $roomInfonum = $roomInfo[0]['game_num'];
            } else {
                $roomInfonum = 0;
            }
            $historysql = "";
            $history = "";
            foreach ($AllMembermoney as $key => $value) {
                $value['name'] = @base64_decode($value['name']);
                $score_board[$value['account_id']] = $value['score'];
                $balance_scoreboardData[] = array('account_id'=>$value['account_id'],'score'=>$value['score'],'name'=>$value['name']);
                $chip = $value['multiples'];
                if ($value['is_banker'] == 1) {
                    $chip = $value['banker_multiples'];
                }
                $historysql .= "(" . $value['account_id'] . "," . $room_id . "," . $value['score'] . "," . time() . "),";
                $history .= "(" . $value['account_id'] . "," . $room_id . "," . $roomInfonum . "," . $value['is_banker'] . ",'" . $value['cards_info'] . "'," . $value['combo_point'] . "," . $value['scorenew'] . "," . $chip . "," . time() . "," . time() . "),";
                $isDetailed = Db::query("select id from t_score_detailed where account_id =" . $value['account_id'] . " and room_id=" . $room_id);
                if (isset($isDetailed[0])) {
                    Db::query("update t_score_detailed set score=" . $value['score'] . "  where account_id =" . $value['account_id'] . " and room_id=" . $room_id);
                } else {
                    $sql = "INSERT INTO t_score_detailed (account_id,room_id,score) VALUES (" . $value['account_id'] . "," . $room_id . "," . $value['score'] . ")";
                    Db::query($sql);
                }
            }
            if ($history) {
                $sql = "INSERT INTO t_history (member_id,room_id,total_num,is_banker,cards,niu_count,scorpe,chip,room_time,createtime) VALUES " . substr($history, 0, strlen($history) - 1);
                Db::query($sql);
            }
            $resultend['data']['score_board'] = $score_board;
            $rommInfo = Db::query("select game_num,total_num from t_room where id =" . $room_id);
            if (isset($rommInfo[0]))
                $rommInfo = $rommInfo[0];
            $resultend['data']['game_num'] = $rommInfo['game_num'];
            $resultend['data']['total_num'] = $rommInfo['total_num'];
            if ($rommInfo['game_num'] == $rommInfo['total_num']) {
                $balance_scoreboard['time'] = time();
                $balance_scoreboard['scoreboard'] = $balance_scoreboardData;
                $balance_scoreboard['game_num'] = $rommInfo['game_num'];
                $resultend['data']['balance_scoreboard'] = $balance_scoreboard;
                model('room')->where(array('id' => $room_id))->update(array('scoreboard' => base64_encode(serialize($balance_scoreboard)), 'roomStatus' => 4, 'end_time' => time()));
                if ($historysql) {
                    $sql = "INSERT INTO t_historyscore (account_id,room_id,score,end_time) VALUES " . substr($historysql, 0, strlen($historysql) - 1);
                    Db::query($sql);
                }
            } else {
                $resultend['data']['balance_scoreboard'] = -1;
                model('room')->where(array('id' => $room_id))->update(array('roomStatus' => 0));
            }
            $resultend['data']['can_break'] = 0;
            $resultend['data']['is_break'] = 0;
            $resultend['data']['banker_id'] = -1;
            $data[10]['to'] = $toMember;
            $data[10]['data'] = $resultend;
            model('member')->where(array('room_id' => $room_id))->update(array('banker_multiples' => 0, 'is_grab' => 0, 'is_banker' => 0, 'cards_info' => "", 'account_status' => 1, 'combo_array' => "", 'banker_multiples' => "", 'card_type' => 0, 'combo_point' => 0, 'multiples' => 0, 'is_multiples' => 0, 'score' => 0));
        }

        echo json_encode($data);
    }

    private function SendMassge($data) {
        require_once "socket_web.php";
        $ws = websocket_open('127.0.0.1', 3120);
        $res = websocket_write($ws, json_encode($data));
        fclose($ws);
    }
    private function getRoomDataByRoomNumber($room_number) {
        $room_at = Db::query('select id from t_room where room_number = :room_number limit 1', ['room_number' => $room_number]);
        if (!is_array($room_at)) {
            die(json_encode([
                'err' => 1,
                'msg' => '房间不存在'
            ]));
        }
        $room_at = $room_at[0]['id'];
        $players = Db::query('select (@rowNum:=@rowNum+1) as serial_num, m.id as account_id, m.nickname, m.photo as headimgurl, m.account_score, m.account_status, m.online_status, m.ticket_checked, m.is_banker, m.cards_info as cards, m.card_type, m.combo_array, m.combo_point, m.multiples, m.banker_multiples from t_member as m, (Select (@rowNum :=0) ) as r where  m.online_status=1 and  m.room_id = :room_id order by m.jointime ASC', ['room_id' => $room_at]);

        array_walk_recursive($players, 'procArraySeri', 'cards');
        array_walk_recursive($players, 'procArraySeri', 'combo_array');

        foreach ($players as $key => $value) {
            $players[$key]['nickname'] = @base64_decode($value['nickname']);
        }
        return $players;
    }
}
