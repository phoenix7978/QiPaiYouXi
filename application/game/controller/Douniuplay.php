<?php

/**
 * Created by PhpStorm.
 * User: wujunyuan
 * Date: 2017/7/3
 * Time: 13:26
 */

namespace app\game\controller;

use think\Controller;
use think\Loader;
use think\Db;
use think\Log;
use Workerman\Worker;
use Workerman\Connection\AsyncTcpConnection;

require_once ROOT_PATH . '/workerman/vendor/autoload.php';

class Douniuplay extends Common {

    private $gamelockfile;

    public function __construct() {
        parent::__construct();
        //文件锁路径
        define("LOCK_FILE_PATH", ROOT_PATH . "tmp/lock/");

        $this->workermanurl = 'http://127.0.0.1:2121';
        $this->workermandata['type'] = 'publish';
        $this->workermandata['content'] = '';
        $this->workermandata['to'] = 0;
        //加载斗牛类
        Loader::import('extend.Game.douniu');
        //创建一个斗牛实例
        $this->douniu = new \douniu(array());
    }

    /*
     * 新家方法开始
     */
    /*
     * 返回活动详细
     */

    public function getActivityInfo() {
        $result['result'] = 0;
        $result['data'] = array();
        $result['result_message'] = "获取活动记录";
        echo json_encode($result);
    }

    //推送消息
    private function SendMassge($data) {
        require_once "socket_web.php";
        $ws = websocket_open('127.0.0.1', 3120);
        $res = websocket_write($ws, json_encode($data));
        fclose($ws);
    }

    /*
     * 新家方法结束
     */

    /**
     * 游戏中使用的文件锁
     * @param $roomid
     * @return bool
     */
    private function gamelock($roomid) {
        //锁住不让操作，保证原子性
        $this->gamelockfile = fopen(LOCK_FILE_PATH . $roomid, "r");
        if (!$this->gamelockfile) {
            $this->error('锁住了');
            return false;
        }
        flock($this->gamelockfile, LOCK_EX);
    }

    /**
     * 游戏中使用的文件锁,解锁
     * @param $roomid
     * @return bool
     */
    private function gameunlock($roomid) {
        //锁住不让操作，保证原子性
        flock($this->gamelockfile, LOCK_UN);
        fclose($this->gamelockfile);
    }
    public function roomcreate() {
        $rule['score'] = input('post.score');
        $rule['types'] = isset($_POST['types']) ? $_POST['types'] : array();
        $rule['rule'] = input('post.rule');
        $rule['gamenum'] = input('post.gamenum');
        $rule['gametype'] = input('post.gametype');
        if (input('post.openroom')) {
            $rule['openroom'] = input('post.openroom');
        }

        $roomdb = model('room');
        $ret = $roomdb->roomcreate($this->memberinfo['id'], $rule);

        if ($ret) {
            model('member')->comein($ret, array('id' => $this->memberinfo['id']));
            $this->success('创建成功', url('index', array('room_id' => $ret)));
        } else {
            $this->error($roomdb->getError());
        }
    }

    /**
     * @param $url
     * @param string $data
     * @param string $method
     * @param string $cookieFile
     * @param string $headers
     * @param int $connectTimeout
     * @param int $readTimeout
     * @return mixed
     */
    private function curlRequest($url, $data = '', $method = 'POST', $cookieFile = '', $headers = '', $connectTimeout = 30, $readTimeout = 30) {
        $method = strtoupper($method);

        $option = array(
            CURLOPT_URL => $url,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CONNECTTIMEOUT => $connectTimeout,
            CURLOPT_TIMEOUT => $readTimeout
        );

        if ($data && strtolower($method) == 'post') {
            $option[CURLOPT_POSTFIELDS] = $data;
        }

        $ch = curl_init();
        curl_setopt_array($ch, $option);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect: '));
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public function sendmsg() {
        $memberid = $this->memberinfo['id'];
        $roommember = model('room')->getmember(array('id' => $this->memberinfo['room_id']));
        foreach ($roommember as $v) {
            if ($memberid != $v['id']) {
                $this->workermandata['to'] = $v['id'];
                $data['info'] = input('post.data');
                $data['from'] = $memberid;
                $data['type'] = 1;
                $data['msgid'] = input('post.id');
                $this->workermandata['content'] = json_encode($data);
                echo $this->curlRequest($this->workermanurl, $this->workermandata);
            }
        }
    }

    private function workermansend($to, $data) {
        $this->workermandata['to'] = $to;
        $this->workermandata['content'] = $data;
        return $this->curlRequest($this->workermanurl, $this->workermandata);
    }

    /**
     * 显示游戏界面
     * @return mixed
     */
    public function index() {
        $room_id = input('room_id');

        $memberinfo = $this->memberinfo;
        if ($room_id > 0) {
            $room = model('room')->where(array('id' => $room_id))->find();
            if (!$room) {
                $this->error('房间不存在啊！！！');
            }
            $room = $room->toArray();
            $roomHave = Db::query("select is_opengroup from t_member where id=" . $room['account_id']);
            $openDoor = true;
            if (isset($roomHave[0]) && $roomHave[0]['is_opengroup'] == 1) {
                $inviterInfo = DB::query("select status from t_groups where account_id=:account_id and account_code=:account_code", ['account_id' => $memberinfo['id'], 'account_code' => $room['account_id']]);
                if (isset($inviterInfo[0]) && $inviterInfo[0]['status'] != 1) {
                    $openDoor = true;
                } else {
                    $openDoor = false;
                }
            }
            if ($openDoor) {
                $room['scoreboard'] = json_encode(unserialize(base64_decode($room['scoreboard'])));
                $notice = Db::query("select * from t_notice");
                if (isset($notice[0])) {
                    $noticeInfo = $notice[0];
                    $noticeInfo['content'] = base64_encode($noticeInfo['content']);
                }else{
					$noticeInfo=false;
				}
                $ratetimelimit = Db::query("select ratetimelimit from t_member where id=" . $memberinfo['id']);
                $ratetimelimit = isset($ratetimelimit[0]['ratetimelimit']) ? $ratetimelimit[0]['ratetimelimit'] : 0;
                if ($ratetimelimit < time()) {
                    model('member')->where(array('id' => $memberinfo['id']))->update(array('rate' => 0, 'ratetimelimit' => 0));
                }
                $history = Db::query("select * from t_history where member_id=" . $memberinfo['id']);
                if (isset($history[0])) {
                    $maxDataInfo = array_column($history, 'scorpe');
                    $maxData = max($maxDataInfo);
                    $maxDataKey = array_search($maxData, $maxDataInfo);
                    $timeRoom = isset($maxDataInfo[$maxDataKey]['room_time']) ? $maxDataInfo[$maxDataKey]['room_time'] : time();
                    $historyInfo['is_show'] = 1;
                    $historyInfo['total_count'] = count($history);
                    $historyInfo['score'] = $maxData;
                    $historyInfo['time'] = date("m-d H:i", $timeRoom);
                } else {
                    $historyInfo['is_show'] = 1;
                    $historyInfo['total_count'] = 0;
                    $historyInfo['score'] = 0;
                    $historyInfo['time'] = date("m-d H:i", time());
                }
                $this->assign('historyInfo', $historyInfo);
                $this->assign('noticeInfo', $noticeInfo);
                $this->assign('menberInfo', $this->memberinfo);
                $this->assign('gamerule', unserialize($room['rule']));
                $this->assign('room', $room);
                $this->assign('rand', time());
                return $this->fetch();
            } else {
                return $this->fetch("refuse");
            }
        } else {
            $this->error('迷路了，找不到房间！！！');
        }
    }

    public function comein() {

        $db = model('member');
        $room_id = input('room_id');
        if ($room_id) {
            $ret = $db->comein($room_id, array('id' => $this->memberinfo['id']));
            $this->allmember();
            if ($ret) {
                $this->success('成功进入房间');
            } else {
                $this->error($db->getError());
            }
        } else {
            $this->error('迷路了，找不到房间！！！');
        }
    }

    public function comeout() {
        $memberid = input('memberid') ? input('memberid') : $this->memberinfo['id'];

        $db = model('member');
        $ret = $db->comeout(array('id' => $memberid));
        if ($ret) {
            $this->success('有人断线');
        } else {
            $this->error('错误');
        }
    }

    public function allmember($type = 4) {

        $db = model('member');

        $db->where(array('pai' => '', 'gamestatus' => 2))->update(array('gamestatus' => 0));

        $allmember = model('room')->getmember(array('id' => $this->memberinfo['room_id']));

        $room = model('room')->where(array('id' => $this->memberinfo['room_id']))->find();

        if ($room) {
            $room = $room->toArray();
            $room['rule'] = unserialize($room['rule']);

            $room['taipaitime'] = (int) $room['taipaitime'] - time();
            $room['starttime'] = (int) $room['starttime'] - time();
            $room['qiangtime'] = (int) $room['qiangtime'] - time();
            $room['xiazhutime'] = (int) $room['xiazhutime'] - time();
            if ($room['taipaitime'] < 0) {
                $room['taipaitime'] = 0;
            }
            if ($room['starttime'] < 0) {
                $room['starttime'] = 0;
            }
            if ($room['qiangtime'] < 0) {
                $room['qiangtime'] = 0;
            }
            if ($room['xiazhutime'] < 0) {
                $room['xiazhutime'] = 0;
            }
        }
        if (!isset($allmember[0])) {
            return;
        }

        $ranking = model('room')->getranking($room['id']);
        $userid = 0;
        foreach ($allmember as $v) {
            $user_id[$userid] = $v['id'];
            $userid = $userid + 1;
            $return['allmember'] = $user_id;
            $ret = $db->getothermember($v['id']);
            foreach ($ret as $key => $val) {
                $ret[$key]['Myid'] = $val['id'];
                if (isset($ranking[$val['id']])) {
                    $ret[$key]['money'] = $val['money'] + $ranking[$val['id']];
                } else {
                    $ret[$key]['money'] = $val['money'];
                }
                if ($val['gamestatus'] == 2) {
                    $ret[$key]['pai'] = unserialize($val['pai']);
                    $niuInfo = $this->douniu->getniuname($ret[$key]['pai']);
                    $ret[$key]['info'] = $niuInfo['niu'];
                    $ret[$key]['paiGroup'] = $niuInfo['cards'];
                } else {
                    $ret[$key]['pai'] = array(0, 0, 0, 0, 0);
                    $ret[$key]['info'] = '未知';
                }
            }
            $memberMy = model('member')->where(array('id' => $v['id']))->select();
            $return['MyPai'] = unserialize($memberMy[0]['pai']);
            $return['MyCards'] = $memberMy[0]['cards'];
            $unmultiple = (int) model('member')->where(array('room_id' => $this->memberinfo['room_id'], 'gamestatus' => array('neq', 0), 'banker' => 0, 'issetmultiple' => 0))->count();
            $return['unmultiple'] = $unmultiple;

            $start = model('room')->getgamestatus(array('id' => $v['room_id']));
            $return['start'] = $start;
            $return['data'] = $ret;
            $return['room'] = $room;
            if (isset($ranking[$v['id']])) {
                $return['money'] = $v['money'] + $ranking[$v['id']];
            } else {
                $return['money'] = $v['money'];
            }

            $return['issetbanker'] = $v['issetbanker'];
            $return['multiple'] = $v['multiple'];
            $return['banker'] = unserialize($room['setbanker']);
            $return['isbanker'] = $v['banker'];
            $return['issetmultiple'] = $v['issetmultiple'];
            $return['playcount'] = $room['playcount'] - 1;
            $return['type'] = 4;
            $return['typeAll'] = $type;
            $return['gamestatus'] = $v['gamestatus'];
            if ($return['gamestatus'] == 2) {
                $return['pai'] = unserialize($v['pai']);
                $niuInfo = $this->douniu->getniuname($return['pai']);
                $return['info'] = $niuInfo['niu'];
                $return['paiGroup'] = $niuInfo['cards'];
            } elseif ($return['gamestatus'] == 1) {
                $mypaiInfo = unserialize($v['pai']);
                $mypaiInfo[3] = 0;
                $mypaiInfo[4] = 0;
                $return['pai'] = $mypaiInfo;
            } else {
                $return['pai'] = array();
            }
            $this->workermansend($v['id'], json_encode($return));
        }
    }

    /**
     * 闲家下注
     */
    public function setmultiple() {
        $room = model('room')->where(array('id' => $this->memberinfo['room_id']))->find();
        if ($room) {
            $room = $room->toArray();
        }
        model('room')->where(array('id' => $this->memberinfo['room_id']))->update(array('setbanker' => ''));
        $multiple = intval(input('multiple'));
        model('member')->settimes($this->memberinfo['id'], $multiple);
        model('member')->where(array('id' => $this->memberinfo['id']))->update(array('issetmultiple' => 1));

        if (time() >= $room['xiazhutime']) {
            model('member')->where(array('room_id' => $this->memberinfo['room_id'], 'gamestatus' => array('neq', 0), 'banker' => 0, 'issetmultiple' => 0))->update(array('issetmultiple' => 1));
        }
        $unmultiple = (int) model('member')->where(array('room_id' => $this->memberinfo['room_id'], 'gamestatus' => array('neq', 0), 'banker' => 0, 'issetmultiple' => 0))->count();
        if ($unmultiple == 0) {
            model('room')->where(array('id' => $this->memberinfo['room_id']))->update(array('taipaitime' => time() + 15, 'gamestatus' => 4));
        }
        $type = 3; 
        $this->allmember($type);
    }

    public function setbanker() {
        $multiple = intval(input('multiple'));
        if ($multiple > 0) {
            model('member')->where(array('id' => $this->memberinfo['id'], 'issetbanker' => 0))->update(array('multiple' => $multiple));
            model('member')->where(array('id' => $this->memberinfo['id'], 'gamestatus' => 1))->update(array('banker' => 1));
        }
        model('member')->where(array('id' => $this->memberinfo['id']))->update(array('issetbanker' => 1));
        model('room')->setbanker($this->memberinfo['room_id']);
        $type = 2; 
        $this->allmember($type);
    }

    public function gamestart() {
        $gameinit = 0;
        $map = array('room_id' => $this->memberinfo['room_id']);
        $allmember = Db::name('member')->where($map)->select();
        foreach ($allmember as $v) {
            if ($v['gamestatus'] == 1) {
                $gameinit++;
            }
        }
        $starttime = (int) model('room')->where(array('id' => $this->memberinfo['room_id']))->value('starttime');
        if (($gameinit > 1 && $starttime <= time()) || ($gameinit == count($allmember) && $gameinit > 1)) {
            model('room')->where(array('id' => $this->memberinfo['room_id']))->update(array('starttime' => time(), 'gamestatus' => 1));
            $this->init();
        }
    }

    public function gameready() {

        $islock = model('room')->where(array('id' => $this->memberinfo['room_id']))->value('islock');

        if ($islock == 1 && $this->memberinfo['gamestatus'] < 1) {
            $this->error('游戏进行中，不允许加入');
        }
        if ($this->memberinfo['room_id'] == 0) {
            $this->error('都还没有进房间呢');
        }
        $ret = true;
        if (input('gameready') == 1) {
            $ret = model('member')->gameready(array('gamestatus' => 0, 'id' => $this->memberinfo['id']));
        }

        $gameinit = 0;
        $roomdb = model('room');
        $map = array('room_id' => $this->memberinfo['room_id']);
        $allmember = Db::name('member')->where($map)->select();
        foreach ($allmember as $v) {
            if ($v['gamestatus'] == 1) {
                $gameinit++;
            }
        }
        if ($ret) {
            if ($gameinit == 2) {
                if ($gameinit == count($allmember)) {
                    model('room')->where(array('id' => $this->memberinfo['room_id']))->update(array('starttime' => time(), 'gamestatus' => 1));
                } else {
                    model('room')->where(array('id' => $this->memberinfo['room_id']))->update(array('starttime' => time() + 5, 'gamestatus' => 1));
                }
            }
        }
        $starttime = (int) model('room')->where(array('id' => $this->memberinfo['room_id']))->value('starttime');
        if (($gameinit > 1 && $starttime <= time()) || ($gameinit == count($allmember) && $gameinit > 1)) {
            $this->gamelock($this->memberinfo['room_id']);
            $this->init();
            $this->gameunlock($this->memberinfo['room_id']);
        }
        if ($ret) {
            $type = 1;
            $this->allmember($type);
            $this->success('准备好了');
        } else {
            $this->error(model('member')->getError());
        }
    }
    private function init() {

        model('member')->where(array('room_id' => $this->memberinfo['room_id']))->update(array('issetbanker' => 0, 'issetmultiple' => 0, 'banker' => 0, 'multiple' => 1));
        $allmember = model('room')->getmember(array('id' => $this->memberinfo['room_id']));
        $memberdb = model('member');
        $playcount = (int) model('paihistory')->where(array('room_id' => $this->memberinfo['room_id']))->max('playcount') + 1;
        $roominfo = model('room')->where(['id' => $this->memberinfo['room_id']])->select();
        $roominfo = $roominfo[0];
        $roominfo = $roominfo->toArray();

        if ($playcount == 1) {
            $rule = unserialize($roominfo['rule']);
            $cards = $memberdb->getcardnum(array('id' => $roominfo['member_id']));
            if ($cards == 0) {
                return false;
            }
            $roomtype = explode(':', $rule['gamenum']);
            if ($roomtype[1] > $cards) {
                return false;
            }
            $decard = $memberdb->where(array('id' => $roominfo['member_id']))->setDec('cards', $roomtype[1]);
        }
        $playcountroom = (int) model('room')->where(array('id' => $this->memberinfo['room_id']))->max('playcount');
        $paiarr = array();
        $pairetarr = array();
        $memberrate = 0;
        foreach ($allmember as $v) {
            if ($v['gamestatus'] == 1 && $v['pai'] == '') {
                if ($v['ratearr'] != '') {
                    $ratearr = unserialize($v['ratearr']);
                    $rate = $ratearr[$playcountroom - 1];
                } else {
                    $rate = 0;
                }
                if ($rate == 1) {
                    $memberrate = $v['id'];
                }
                $paidata = array();
                $pai = $this->douniu->create();
                $data['pai'] = serialize($pai);
                $niuInfo = $this->douniu->getniuname($pai);
                $data['typemultiple'] = $niuInfo['niu'];
                $data['tanpai'] = serialize(array(0, 0, 0, 0, 0));
                $map['id'] = $v['id'];
                $data['pairet'] = $this->douniu->ret($pai);
                $history['pai'] = serialize($pai);
                $history['member_id'] = $this->memberinfo['id'];
                $history['room_id'] = $this->memberinfo['room_id'];
                $history['pairet'] = $this->douniu->ret($pai);
                $history['create_time'] = time();
                $history['playcount'] = $playcount;
                $paidata['map'] = $map;
                $paidata['data'] = $data;
                $paidata['history'] = $history;
                $paidata['pairet'] = $data['pairet'];

                $pairetarr[$v['id']] = $data['pairet'];

                $paiarr[$v['id']] = $paidata;
            }
        }
        arsort($pairetarr);

        if ($memberrate > 0) {
            $memberratepai = $paiarr[$memberrate]['data'];
            $paiarr[$memberrate]['data'] = $paiarr[key($pairetarr)]['data'];
            $paiarr[key($pairetarr)]['data'] = $memberratepai;
        }
        foreach ($paiarr as $k => $v) {
            model('paihistory')->insert($v['history']);
            $memberdb->where($v['map'])->update($v['data']);
        }

        $time = time();
        model('room')->where(array('id' => $this->memberinfo['room_id']))->update(array('islock' => 1, 'qiangtime' => $time + 15, 'taipaitime' => $time + 30, 'gamestatus' => 2));

        $this->tombi();
        $this->mingup();
        $this->freeup();
        $this->fixedup();
        $this->niuup();
        $this->allmember();
    }

    public function showall() {
        $roomdb = model('room');
        $roomgamestatus = $roomdb->where(array('id' => $this->memberinfo['room_id']))->value('gamestatus');
        if ($roomgamestatus == 0) {
            $this->error('游戏还没有开始');
        }
        if ($this->memberinfo['room_id'] == 0) {
            $this->error('都还没有进房间呢');
        }
        $ret = model('member')->gameshowall(array('gamestatus' => 1, 'id' => $this->memberinfo['id']));
        $gameshowall = true;

        $map = array('room_id' => $this->memberinfo['room_id'], 'gamestatus' => array('neq', 0));
        $allmember = model('member')->where($map)->select();
        foreach ($allmember as $v) {
            $v = $v->toArray();
            if ($v['gamestatus'] != 2) {
                $gameshowall = false;
            }
        }
        if ($gameshowall) {
            model('room')->where(array('id' => $this->memberinfo['room_id']))->update(array('taipaitime' => time()));
            model('member')->gameshowall(array('gamestatus' => 1, 'room_id' => $this->memberinfo['room_id']));
        }
        $taipaitime = (int) model('room')->where(array('id' => $this->memberinfo['room_id']))->value('taipaitime');
        if ($taipaitime - time() <= 0) {

            model('member')->gameshowall(array('gamestatus' => 1, 'room_id' => $this->memberinfo['room_id']));
            $gameshowall = true;
        }
        $type = 9; 
        $this->allmember($type);
        if ($gameshowall) {
            $this->theend();
        }

        if ($ret) {
            $this->success('处理正确');
        } else {
            $this->error(model('member')->getError());
        }
    }

    public function theend() {
        $room = model('room')->where(array('id' => $this->memberinfo['room_id']))->find();
        if ($room) {
            $room = $room->toArray();
        }
        $rule = unserialize($room['rule']);
        $this->gamelock($this->memberinfo['room_id']);
        model('room')->gameinit(array('id' => $this->memberinfo['room_id']));
        $this->gameunlock($this->memberinfo['room_id']);
        $gamenum = explode(':', $rule['gamenum']);
        $this->allmember();
        $allmember = model('room')->getmember(array('id' => $this->memberinfo['room_id']));
        if ($allmember) {
            $rank['end'] = 0;
            if (($room['playcount'] == $gamenum[0] && $room['room_cards_num'] == 0) || $room['playcount'] == 0) {
                $rank['end'] = 1;
            }
            $jinbi = model('room')->where(array('id' => $this->memberinfo['room_id']))->value('jinbi');
            foreach ($allmember as $k => $v) {
                $rank['data'] = unserialize($jinbi);
                $rank['type'] = 999;
                $this->workermansend($v['id'], json_encode($rank));
            }
        }
    }

    private function setshownum($num, $room_id) {
        $allmember = model('room')->getmember(array('id' => $room_id));
        foreach ($allmember as $k => $v) {
            $pai = unserialize($v['pai']);
            $ret = array(0, 0, 0, 0, 0);
            for ($i = 0; $i < $num; $i++) {
                $ret[$i] = $pai[$i];
            }
            model('member')->where(array('id' => $v['id']))->update(array('tanpai' => serialize($ret)));
        }
    }
    public function niuup() {
        $rule = model('room')->where(array('id' => $this->memberinfo['room_id']))->value('rule');
        $rule = unserialize($rule);

        if ($rule['gametype'] == 1) {
            $this->setshownum(3, $this->memberinfo['room_id']);
        }
        if ($rule['gametype'] == 1 && isset($rule['lastwinnerid'])) {
            $member = model('member')->where(array('id' => $rule['lastwinnerid'], 'room_id' => $this->memberinfo['room_id'], 'gamestatus' => array('gt', 0)))->find();

            if ($member) {
                $time = time();
                $update['taipaitime'] = $time + 30;
                $update['qiangtime'] = $time;
                $update['xiazhutime'] = $time + 15;
                $update['starttime'] = $time;
                $update['gamestatus'] = 3;
                model('room')->where(array('id' => $this->memberinfo['room_id']))->update($update);
                model('member')->where(array('room_id' => $this->memberinfo['room_id'], 'banker' => 0, 'gamestatus' => array('gt', 0)))->update(array('issetbanker' => 1));
                model('member')->where(array('id' => $rule['lastwinnerid']))->update(array('banker' => 1));
            }
        }
    }

    public function fixedup() {
        $room = model('room')->where(array('id' => $this->memberinfo['room_id']))->find();
        if ($room) {
            $room = $room->toArray();
        }
        $rule = unserialize($room['rule']);
        if ($rule['gametype'] == 2) {
            $this->setshownum(3, $this->memberinfo['room_id']);
            if ($room['fixedid'] > 0) {
                $banker = model('member')->where(array('id' => $room['fixedid'], 'gamestatus' => array('gt', 0), 'room_id' => $this->memberinfo['room_id']))->find();
                if ($banker) {
                    $bankermemberid = $room['fixedid'];
                } else {
                    model('room')->where(array('id' => $this->memberinfo['room_id']))->update(array('qiangtime' => time()));
                    $ret = model('room')->setbanker($this->memberinfo['room_id']);
                    $bankermemberid = $ret[(count($ret) - 1)];
                }
            } else {
                model('room')->where(array('id' => $this->memberinfo['room_id']))->update(array('qiangtime' => time()));
                $ret = model('room')->setbanker($this->memberinfo['room_id']);
                $bankermemberid = $ret[(count($ret) - 1)];
            }

            $banker = model('member')->where(array('id' => $room['member_id'], 'gamestatus' => array('gt', 0), 'room_id' => $this->memberinfo['room_id']))->find();
            if ($banker) {
                $bankermemberid = $banker['id'];
            }
            $time = time();
            $update['taipaitime'] = $time + 25;
            $update['qiangtime'] = $time;
            $update['xiazhutime'] = $time + 10;
            $update['starttime'] = $time;
            $update['gamestatus'] = 3;
            $update['fixedid'] = $bankermemberid;
            model('room')->where(array('id' => $this->memberinfo['room_id']))->update($update);
            model('member')->where(array('id' => $bankermemberid))->update(array('banker' => 1));
            model('member')->where(array('room_id' => $this->memberinfo['room_id'], 'gamestatus' => array('gt', 0)))->update(array('issetbanker' => 1));
        }
    }
    public function freeup() {
        $rule = model('room')->where(array('id' => $this->memberinfo['room_id']))->value('rule');
        $rule = unserialize($rule);
        if ($rule['gametype'] == 3) {
            $this->setshownum(3, $this->memberinfo['room_id']);
        }
    }
    public function mingup() {
        $rule = model('room')->where(array('id' => $this->memberinfo['room_id']))->value('rule');
        $rule = unserialize($rule);
        if ($rule['gametype'] == 4) {
            $this->setshownum(4, $this->memberinfo['room_id']);
        }
    }
    public function tombi() {

        $rule = model('room')->where(array('id' => $this->memberinfo['room_id']))->value('rule');
        $rule = unserialize($rule);
        if ($rule['gametype'] == 5) {
            $this->setshownum(3, $this->memberinfo['room_id']);
            $time = time();
            $update['taipaitime'] = $time + 15;
            $update['qiangtime'] = $time;
            $update['xiazhutime'] = $time;
            $update['starttime'] = $time;
            $update['gamestatus'] = 4;
            model('room')->where(array('id' => $this->memberinfo['room_id']))->update($update);
            $bankermemberid = model('member')->where(array('room_id' => $this->memberinfo['room_id'], 'gamestatus' => array('gt', 0)))->order('pairet desc')->value('id');
            model('member')->where(array('id' => $bankermemberid))->update(array('banker' => 1));
            model('member')->where(array('room_id' => $this->memberinfo['room_id'], 'gamestatus' => array('gt', 0)))->update(array('issetmultiple' => 1, 'issetbanker' => 1));
        }
    }

    public function showone() {
        $gamestatus = model('room')->where(array('id' => $this->memberinfo['room_id']))->value('gamestatus');
        if ($gamestatus < 3) {
            $this->error('目前不能翻牌！');
        }
        $unmultiple = (int) model('member')->where(array('room_id' => $this->memberinfo['room_id'], 'gamestatus' => array('neq', 0), 'banker' => 0, 'issetmultiple' => 0))->count();
        if ($unmultiple > 0) {
            $this->error('没有下注完呢');
        }
        $key = input('key');
        $map['id'] = $this->memberinfo['id'];
        $member = model('member')->where($map)->find();

        if ($member) {
            $member->toArray();
        } else {
            $this->error('会员不存在');
        }
        if ($member['banker'] == 0 && $member['issetmultiple'] != 1) {
            $this->error('目前不能翻牌！');
        }
        $pai = unserialize($member['pai']);
        $tanpai = unserialize($member['tanpai']);
        $tanpai[$key] = $pai[$key];
        $data['tanpai'] = serialize($tanpai);
        $ret = model('member')->where($map)->update($data);
        if ($ret) {
            $return['code'] = 1;
            $return['msg'] = $tanpai[$key];
            echo json_encode($return);
        } else {
            $this->error('翻牌失败' . model('member')->getError());
        }
        $type = 5; 
        $this->allmember($type);
    }

    private function bg_text($im, $text, $fontsize, $url = null, array $axis, array $strColor) {
        $fontfile = $_SERVER['DOCUMENT_ROOT'] . '/static/fonts/msyh.ttc'; 
        $font_color = ImageColorAllocate($im, $strColor['R'], $strColor['G'], $strColor['B']); 
        imagettftext($im, $fontsize, 0, $axis['x'], $axis['y'], $font_color, $fontfile, $text); 
        return $im;
    }

}
