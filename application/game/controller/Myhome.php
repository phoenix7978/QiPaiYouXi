<?php

namespace app\game\controller;

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/19
 * Time: 18:02
 */
use think\Controller;
use think\Db;

class Myhome extends Common {

    public function index() {
        //显示个人主页
        return $this->fetch();
    }

    public function homepage() {
        $this->assign('menberInfo', $this->memberinfo);
        //显示个人中心
        return $this->fetch();
    }

    public function tutorial() {
        //显示个人中心
        return $this->fetch();
    }

    public function test() {
        Db::startTrans();
        $res = DB::query('select * from t_ad where id=:id FOR UPDATE', ['id' => 4]);
        if ($res[0]['id']) {
            $exec = DB::execute('update t_ad set title=:title where id=:id', ['title' => '广告名称', 'id' => $res[0]['id']]);
            if ($exec) {
                Db::commit();
                echo "事务已提交<br>";
            } else {
                Db::rollback();
                echo "事务回滚";
            }
        }
        echo DB::execute('update t_ad set title=:title where id=:id', ['title' => '广告栏目', 'id' => $res[0]['id']]);
    }

    public function gameScoreDetail() {
        $gameinfo = [
            '5' => [
                'ticket_type' => [
                    1 => 1,
                    2 => 2
                ],
                'banker_mode' => [
                    1 => '自由抢庄',
                    2 => '明牌抢庄',
                    3 => '牛牛上庄',
                    4 => '通比牛牛',
                    5 => '固定庄家',
                ],
                'score_type' => [
                    1 => 1,
                    2 => 3,
                    3 => 5,
                    6 => 2,
                    7 => 4
                ],
                'rule_type' => [
                    1 => '牛牛x3牛九x2牛八x2',
                    2 => '牛牛x4牛九x3牛八x2牛七x2'
                ],
            ],
            '9' => [
                'ticket_type' => [
                    1 => 2,
                    2 => 4
                ],
                'banker_mode' => [
                    1 => '自由抢庄',
                    2 => '明牌抢庄',
                    3 => '牛牛上庄',
                    4 => '通比牛牛',
                    5 => '固定庄家',
                ],
                'score_type' => [
                    1 => 1,
                    2 => 3,
                    3 => 5,
                    6 => 2,
                    7 => 4
                ],
                'rule_type' => [
                    1 => '牛牛x3牛九x2牛八x2',
                    2 => '牛牛x4牛九x3牛八x2牛七x2'
                ],
            ]
        ];
        $dealer_num = input('get.dealer_num');
        $game_type = intval(input('get.game_type'));
        $room_number = strval(input('get.room_number'));
        $data = Db::query("select room_number, game_type,total_num,ticket_type,banker_mode,score_type,rule_type, FROM_UNIXTIME(starttime, '%m-%d %H:%i') as startime, FROM_UNIXTIME(end_time, '%m-%d %H:%i') as endtime, scoreboard from t_room where room_number=:room_number and game_type=:game_type", ['room_number' => $room_number, 'game_type' => $game_type]);
        if (is_array($data)) {
            $data = $data[0];
            $rule_text = $data['total_num']."局/".$gameinfo[$data['game_type']]['ticket_type'][$data['ticket_type']]."张房卡,".$gameinfo[$data['game_type']]['banker_mode'][$data['banker_mode']].",底分".$gameinfo[$data['game_type']]['score_type'][$data['score_type']]."分,".$gameinfo[$data['game_type']]['rule_type'][$data['rule_type']];
			unset($data['ticket_type'],$data['banker_mode'],$data['total_num'],$data['score_type'],$data['rule_type']);
            $data['scoreboard'] = unserialize(base64_decode($data['scoreboard']));
            $data['scoreboard'] = $data['scoreboard']['scoreboard'];
            foreach ($data['scoreboard'] as &$v) {
                if (isset($v['account_id'])) {
                    $m = model('member')->where(['id' => $v['account_id']])->find();
                    $v['headimgurl'] = $m->photo;
                    $v['account_code'] = $v['account_id'];
                    unset($v['account_id']);
                }
            }
        }
        $player_array = Db::query(
                        'SELECT h.room_id, r.total_num, sum(h.scorpe) AS countscorpe, h.total_num AS game_num FROM	t_room AS r,	t_history AS h WHERE	r.room_number = :room_number AND h.room_id = r.id GROUP BY	h.total_num', ['room_number' => $room_number]
        );
        if (is_array($player_array)) {
            foreach ($player_array as $k => &$v) {
                $v['player_cards'] = Db::query(
                                'SELECT m.nickname AS name, h.chip, h.scorpe as score, h.is_banker, h.cards as player_cards, CASE WHEN h.scorpe=0 THEN 0 ELSE 1 END as is_join,	CASE WHEN h.niu_count=0 then \'无牛\' WHEN h.niu_count>0 and h.niu_count<10 THEN CONCAT(\'牛\',h.niu_count) WHEN h.niu_count=10 THEN \'牛牛\' WHEN h.niu_count=11 THEN \'五花牛\' WHEN h.niu_count=12 THEN \'炸弹牛\' WHEN h.niu_count=13 THEN \'五小牛\' END as card_type_str FROM	t_history AS h,	t_member AS m WHERE	h.room_id = :room_id AND h.total_num = :total_num AND h.member_id = m.id', ['room_id' => $v['room_id'], 'total_num' => $v['game_num']]
                );
                if (is_array($v['player_cards'])) {
                    foreach ($v['player_cards'] as $k => &$val) {
                        if (isset($val['player_cards'])) {
                            !empty($val['player_cards']) ? $val['player_cards'] = unserialize($val['player_cards']) : $val['player_cards'] = '未参与该局游戏';
                        }
                    }
                }
            }
        }
        $balance_board = Db::query(
                        "select h.account_id as account_code, h.score, m.nickname,m.photo as headimgurl  from t_historyscore as h, t_room as r, t_member as m where r.game_type=:game_type and r.room_number=:room_number and h.room_id=r.id and h.account_id=m.id", ['room_number' => $room_number, 'game_type' => $game_type]
        );
        $scoreInfo = json_encode([
            'start_time' => $data['startime'],
            'end_time' => $data['endtime'],
            'rule_text' => $rule_text,
            'player_array' => $player_array,
            'balance_board' => $balance_board
        ]);
        $this->assign('gameScoreDetail', $data);
        $this->assign('scoreInfo', $scoreInfo);
        $this->assign('menberInfo', $this->memberinfo);
        return $this->fetch();
    }

    public function getGameScoreList() {
        $account_id = intval(input('post.account_id'));
        $game_type = intval(input('post.game_type'));
        $page = intval(input('post.page'));
        $page >= 1 ? $page = $page : $page = 1;
        $scores = DB::query("select count(*) as rowsNum from t_historyscore as h left join t_room as r on r.id=h.room_id where h.account_id=:account_id", ['account_id' => $account_id]);
        $startrow = $page * 10 - 10;
        $endrow = $page * 10;
        $scores == false ? $scores = 0 : $scores = $scores[0]['rowsNum'];
        $sql = "select h.score, r.room_number, FROM_UNIXTIME(r.end_time, '%m-%d %H:%i') as time from t_historyscore as h, t_room as r  where h.account_id=:account_id and r.id=h.room_id and r.game_type=:game_type order by r.end_time desc limit :startrow, :endrow";
        $data['data'] = DB::query($sql, ['game_type' => $game_type, 'account_id' => $account_id, 'startrow' => $startrow, 'endrow' => $endrow]);
        $data['result'] = 0;
        $data['result_message'] = "获取活动记录";
        die(json_encode($data));
    }

    public function roomcardinfo() {
        $data['result'] = 0;
        $data['data'] = array();
        $data['result_message'] = "暂无数据";
        echo json_encode($data);
    }
    public function groupMember() {

        $this->assign('menberInfo', $this->memberinfo);
        return $this->fetch();
    }

    public function roomlist() {
        $this->assign('menberInfo', $this->memberinfo);
        return $this->fetch();
    }

    public function openGroup() {
        $dataPost = json_decode(file_get_contents('php://input'), true);
        $account_id = isset($dataPost['account_id']) ? $dataPost['account_id'] : null;
        $type = isset($dataPost['type']) ? $dataPost['type'] : null;
        $dealer_num = isset($dataPost['dealer_num']) ? $dataPost['dealer_num'] : null;
        $memberInfo = Db::query("select * from t_member where id =" . $account_id);
        if (isset($memberInfo[0])) {
            if ($type == 1) {
                $mycards = $memberInfo[0]['cards'];
                $opengroup = $memberInfo[0]['is_opengroup'];
                if ($mycards >= 100) {
                    $cardsNew = $memberInfo[0]['cards'] - 100;
                    $opened = model('member')->where(array('id' => $account_id, 'is_opengroup' => 0))->where('cards', '>=', 100)->update(array('is_opengroup' => 1, 'cards' => $cardsNew));
                    if ($opened) {
                        $memSelf = [
                            'account_id' => $memberInfo[0]['id'],
                            'group_winer' => 1,
                            'account_code' => $memberInfo[0]['id'],
                            'nickname' => $memberInfo[0]['nickname'],
                            'headimgurl' => $memberInfo[0]['photo'],
                            'creatime' => time()
                        ];
                        Db::table('t_groups')->insert($memSelf);
                        $result['result'] = 0;
                        $result['type'] = 1;
                        $result['result_message'] = "开通成功";
                    } else {
                        $result['result'] = 0;
                        $result['type'] = 0;
                        $result['result_message'] = "开通失败";
                    }
                } else {
                    $result['result'] = 1;
                    $result['type'] = 0;
                    $result['result_message'] = "房卡不足";
                }
            } else {
                model('member')->where(array('id' => $account_id, 'is_opengroup' => 1))->update(array('is_opengroup' => 0));
                Db::table('t_groups')->where(array('account_id' => $account_id))->delete();
                $result['result'] = 0;
                $result['type'] = 0;
                $result['result_message'] = "取消成功";
            }
        } else {
            $result['result'] = 1;
            $result['type'] = 0;
            $result['result_message'] = "用户不存在";
        }
        echo json_encode($result);
    }

    public function groupInvite() {
        $code = (int) input('get.code');
        $inviter = DB::query('select * from t_groups where account_code=:code and group_winer=1 limit 1', ['code' => $code]);
        if (isset($inviter[0])) {
            $inviteInfo = $inviter[0];
        } else {
            $inviteInfo = false;
        }
        $this->assign('inviteInfo', $inviteInfo);
        $this->assign('menberInfo', $this->memberinfo);
        return $this->fetch();
    }

}
